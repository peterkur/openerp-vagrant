# OpenERP installation
$OPENERP_VERSION = "6.1"
class openerp {
    exec { "apt-add-repo-openerp":
        command => "apt-add-repository http://nightly.openerp.com/$OPENERP_VERSION/nightly/deb/",
        require => Package['python-software-properties']
    }
    package { 
	   ["python-dateutil", "python-feedparser", "python-gdata", "python-ldap", "python-libxslt1", "python-lxml", "python-mako", "python-openid", "python-psycopg2", "python-pybabel", "python-pychart", "python-pydot", "python-pyparsing", "python-reportlab", "python-simplejson", "python-tz", "python-vatnumber", "python-vobject", "python-webdav", "python-werkzeug", "python-xlwt", "python-yaml", "python-zsi"]:
             ensure => installed;
    }
    exec { "wget-openerp":
      command => "wget http://nightly.openerp.com/$OPENERP_VERSION/nightly/deb/openerp_$OPENERP_VERSION-latest-1_all.deb",
      require => [ Package["python-dateutil"], Package["python-feedparser"], Package["python-gdata"], Package["python-ldap"], Package["python-libxslt1"], Package["python-lxml"], Package["python-mako"], Package["python-openid"], Package["python-psycopg2"], Package["python-pybabel"], Package["python-pychart"], Package["python-pydot"], Package["python-pyparsing"], Package["python-reportlab"], Package["python-simplejson"], Package["python-tz"], Package["python-vatnumber"], Package["python-vobject"], Package["python-webdav"], Package["python-werkzeug"], Package["python-xlwt"], Package["python-yaml"], Package["python-zsi"] ]
    }
    
    exec { "apt-install-openerp":
	command => "dpkg -i openerp_$OPENERP_VERSION-latest-1_all.deb",
	require => Exec["wget-openerp"]
    }
    file { "/etc/openerp/openerp-server.conf":
        source => "$PROJ_DIR/puppet/files/etc/openerp/openerp-server.conf",
        owner => "root", group => "root", mode => 0644,
        ensure => file,
        require => Exec["apt-install-openerp"];
    }
    
    file { "/home/vagrant/bin":
	ensure => directory,
	owner  => "vagrant",
	group  => "vagrant",
	mode => 750,
    }

    file { "/home/vagrant/bin/base_quality_interrogation.py":
        ensure => file,
        source => "$PROJ_DIR/puppet/files/bin/base_quality_interrogation.py",
        replace => false,
        require => File["/home/vagrant/bin"];
    }
    file { "/home/vagrant/bin/bqi":
        ensure => file,
        source => "$PROJ_DIR/puppet/files/bin/bqi",
        replace => false,
        require => File["/home/vagrant/bin"];
    }
    exec { "pgsql-openerp":
	   command => 'sudo -u postgres psql -c "ALTER USER openerp WITH PASSWORD \'openerp\';"',
	   require => Exec["apt-install-openerp"];
    }
}
